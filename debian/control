Source: libvmod-redis
Maintainer: Varnish Package Maintainers <team+varnish-team@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Section: web
Priority: optional
Build-Depends:
 autoconf-archive,
 debhelper-compat (= 13),
 libev-dev,
 libhiredis-dev,
 libvarnishapi-dev (>= 6.6.0~),
 pkg-config,
 python3-docutils,
Standards-Version: 4.6.1.0
Vcs-Browser: https://salsa.debian.org/varnish-team/libvmod-redis
Vcs-Git: https://salsa.debian.org/varnish-team/libvmod-redis.git
Homepage: https://github.com/carlosabalde/libvmod-redis/
Rules-Requires-Root: no

Package: varnish-redis
Architecture: any
Depends:
 ${Varnish:ABI},
 ${misc:Depends},
 ${shlibs:Depends},
Description: access Redis servers from VCL
 VMOD using the synchronous hiredis library API to access Redis servers from
 VCL.
 .
 Highlights:
  * Full support for execution of Lua scripts (i.e. EVAL command), including
    optimistic automatic execution of EVALSHA commands.
  * All Redis reply data types are supported, including partial support to
    access to components of simple (i.e. not nested) array replies.
  * Redis pipelines are not (and won't be) supported. Lua scripting, which is
    fully supported by the VMOD, it's a much more flexible alternative to
    pipelines for atomic execution and minimizing latency. Pipelines are hard
    to use and error prone, specially when using the WATCH command.
  * Support for classic Redis deployments using multiple replicated Redis
    servers and for clustered deployments based on Redis Cluster.
  * Support for multiple databases and multiple Redis connections, local to
    each Varnish worker thread, or shared using one or more pools.
  * Support for smart command execution, selecting the destination server
    according with the preferred role (i.e. master or slave) and with distance
    and healthiness metrics collected during execution.
  * Support for Redis Sentinel, allowing automatic discovery of sick / healthy
    servers and changes in their roles.
